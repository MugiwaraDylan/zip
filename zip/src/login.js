import React from 'react'
import * as ReactBootStrap from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './login.css';
const login = () => {
    return (
        <div>
            <ReactBootStrap.Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <ReactBootStrap.Navbar.Brand><Link to='/' style={{ color: 'white' }}><b>Frontyr</b></Link></ReactBootStrap.Navbar.Brand>
            </ReactBootStrap.Navbar>


           <div className="loginBox">
            <ReactBootStrap.Form>
                <p className="logo"><b>Frontyr</b></p>
                <ReactBootStrap.Form.Group controlId="formBasicEmail">
                    <ReactBootStrap.Form.Label></ReactBootStrap.Form.Label>
                    <ReactBootStrap.Form.Control type="email" placeholder="Enter email address" />
                    <ReactBootStrap.Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </ReactBootStrap.Form.Text>
                </ReactBootStrap.Form.Group>

                <ReactBootStrap.Form.Group controlId="formBasicPassword">
                    <ReactBootStrap.Form.Label className="label"></ReactBootStrap.Form.Label>
                    <ReactBootStrap.Form.Control type="password" placeholder="Password" />
                </ReactBootStrap.Form.Group>
                <ReactBootStrap.Form.Group controlId="formBasicCheckbox">
                    <ReactBootStrap.Form.Check type="checkbox" label="remember me" />
                </ReactBootStrap.Form.Group>
                <ReactBootStrap.Button variant="primary" type="submit" to>
                <Link to='/home.js'  style={{ color: 'white' }}>Login</Link>
            </ReactBootStrap.Button>
            </ReactBootStrap.Form>
            </div>
        </div>
    )
}

export default login

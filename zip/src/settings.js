import { Link } from 'react-router-dom';
import Header from './header.js';
import * as ReactBootStrap from 'react-bootstrap';
import Footer from './footer.js';

const settings = () => {
    return (
        <div>
            <Header />
            <ReactBootStrap.Container>

                <ReactBootStrap.Row>
                    <ReactBootStrap.Col sm={4} className="block">
                        BLOCK1
                    </ReactBootStrap.Col>

                    <ReactBootStrap.Col sm={4} className="block">
                        BLOCK2
                    </ReactBootStrap.Col>

                    <ReactBootStrap.Col sm={4} className="block">
                        BLOCK3
                    </ReactBootStrap.Col>

                </ReactBootStrap.Row>
            </ReactBootStrap.Container>
            <Footer />
        </div>
    )
}

export default settings

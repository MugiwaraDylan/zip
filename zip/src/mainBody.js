import React from 'react'
import * as ReactBootStrap from 'react-bootstrap';
import './index.css';
import Cards from './cards.js'

const mainBody = () => {
    return (
      <div className="body">
        
        <Cards />
        
      </div>
    );
}

export default mainBody

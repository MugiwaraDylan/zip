import React from 'react';
import CardItem from './cardItem.js';
import './cards.css';
import InfoCard from './infoCard.js';

function cards() {
    return (
        <div className="cards">
            <h1>Courses</h1>
            <div className="cards__container">
                <div className="cards__wrapper">
                    <ul className="cards__items">
                        <CardItem
                            src={require('./images/img-9.jpg').default}
                            text="Designer"
                            label="Courses"
                            path='#'

                        />
                        <CardItem
                            src={require('./images/img-9.jpg').default}
                            text="Hacker"
                            label="Courses"
                            path='#'

                        />
                        <CardItem
                            src={require('./images/img-9.jpg').default}
                            text="Influencer"
                            label="Courses"
                            path='#'

                        />
                    </ul>
                    <br /><br />
                    <h1>Updates</h1>
                    <br />
                    <ul className="cards__items">
                        <InfoCard
                            
                            text="New video for Maya has been uploaded!"
                            
                            path='#'

                        />
                        <InfoCard
                            
                            text="New video for Unity has been uploaded!"
                            
                            path='#'

                        />
                        <InfoCard
                            
                            text="New video for ReactJS has been uploaded!"
                            
                            path='#'

                        />

                    </ul>
                    <ul className="cards__items">
                        <InfoCard
                            
                            text="New video for Maya has been uploaded!"
                            
                            path='#'

                        />
                        <InfoCard
                            
                            text="New video for Concepting has been uploaded!"
                            
                            path='#'

                        />
                        <InfoCard
                            
                            text="New video for C# & Unity has been uploaded!"
                            
                            path='#'

                        />

                    </ul>
                </div>
            </div>
        </div>
    )
}

export default cards

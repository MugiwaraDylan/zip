import React from 'react'
import * as ReactBootStrap from 'react-bootstrap';
import {Link} from 'react-router-dom';
const header = () => {
    return (
        <div className="outerHeader">
          <ReactBootStrap.Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <ReactBootStrap.Navbar.Brand><Link to='/home.js' style={{color:'white'}}><b>Frontyr</b></Link></ReactBootStrap.Navbar.Brand>
  <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
    <ReactBootStrap.Nav className="mr-auto">
      {/* <ReactBootStrap.Nav.Link href="#features">Forums</ReactBootStrap.Nav.Link>
      <ReactBootStrap.Nav.Link href="#pricing">Lectures</ReactBootStrap.Nav.Link> */}
      {/* <ReactBootStrap.NavDropdown title="Dropdown" id="collasible-nav-dropdown">
        <ReactBootStrap.NavDropdown.Item href="#action/3.1">Action</ReactBootStrap.NavDropdown.Item>
        <ReactBootStrap.NavDropdown.Item href="#action/3.2">Another action</ReactBootStrap.NavDropdown.Item>
        <ReactBootStrap.NavDropdown.Item href="#action/3.3">Something</ReactBootStrap.NavDropdown.Item>
        <ReactBootStrap.NavDropdown.Divider />
        <ReactBootStrap.NavDropdown.Item href="#action/3.4">Separated link</ReactBootStrap.NavDropdown.Item>
      </ReactBootStrap.NavDropdown> */}
    </ReactBootStrap.Nav>
    <ReactBootStrap.Nav>
    <ReactBootStrap.Nav.Link><Link to='/forums.js' style={{color:'white'}}>Forums</Link></ReactBootStrap.Nav.Link>
      <ReactBootStrap.Nav.Link><Link to='/lectures.js' style={{color:'white'}}>Lectures</Link></ReactBootStrap.Nav.Link>
      <ReactBootStrap.Nav.Link><Link to='#' style={{color:'white'}}>Settings</Link></ReactBootStrap.Nav.Link>
      <ReactBootStrap.Nav.Link eventKey={2}><Link to='#' style={{color:'white'}}>Account</Link></ReactBootStrap.Nav.Link>
    </ReactBootStrap.Nav>
  </ReactBootStrap.Navbar.Collapse>
</ReactBootStrap.Navbar>  
        </div>
    )
}

export default header

import React from 'react';
import CardItem from './cardItem.js';
import './cards.css';


function cards() {
    return (
        <div className="cards">
            <h1>Recent News</h1>
            <div className="cards__container">
                <div className="cards__wrapper">
                    <ul className="cards__items">
                        <CardItem
                            src= {require('./images/img-9.jpg').default}
                            text="Covid update may 28th: school is re-opened"
                            label='News'
                            path='#'

                        />
                        <CardItem
                            src= {require('./images/img-9.jpg').default}
                            text="Covid update may 3rd: back to school SoonTM"
                            label='News'
                            path='#'

                        />
                    </ul>
                    <br/><br/>
                    <h1>Recent Posts</h1>
                    <br/>
                    <ul className="cards__items">
                        <CardItem
                            src= {require('./images/img-9.jpg').default}
                            text="New video for Maya has been uploaded!"
                            label='Latest'
                            path='./lectures.js'

                        />
                        <CardItem
                            src= {require('./images/img-9.jpg').default}
                            text="New video for ReachJS has been uploaded!"
                            label='Latest'
                            path='./lectures.js'

                        />
                        <CardItem
                            src= {require('./images/img-9.jpg').default}
                            text="New video for Concepting has been uploaded!"
                            label='Latest'
                            path='./lectures.js'

                        />
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default cards


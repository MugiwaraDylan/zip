import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './header.js';
import Footer from './footer.js';
// import EmptyBody from './emptyBody.js';
import MainBody from './mainBody.js';
// import { Router } from 'react-router';
import Account from './account.js';
import Lectures from './lectures.js';
import Settings from './settings.js';
import Forums from './forums.js';
import Home from './home.js';
import Login from './login.js';

function App() {
  return (
    <Router>
      <div className="App">

        <Route path='/' exact render={() => (
          <>
            <Login />
          </>
        )} />

        <Route path='/account.js' component={Account} />
        <Route path='/forums.js' component={Forums} />
        <Route path='/settings.js' component={Settings} />
        <Route path='/lectures.js' component={Lectures} />
        <Route path='/home.js' component={Home} />
      </div>
    </Router>
  );
}

export default App;

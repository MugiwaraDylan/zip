import { Link } from 'react-router-dom';
import Header from './header.js';
import * as ReactBootStrap from 'react-bootstrap';
import Footer from './footer.js';
const forums = () => {
    return (
        <div>
            <Header />
            <ReactBootStrap.Container>

                <ReactBootStrap.Row>
                    <ReactBootStrap.Col sm={8} className="block">
                        Forum
                    </ReactBootStrap.Col>

                    <ReactBootStrap.Col sm={4} className="block">
                        Recent Notifications
                    </ReactBootStrap.Col>
                    

                </ReactBootStrap.Row>
            </ReactBootStrap.Container>
            <Footer />
        </div>
    )
}

export default forums

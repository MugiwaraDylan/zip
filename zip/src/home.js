import React from 'react';
import Header from './header.js';
import Footer from './footer.js';
import MainBody from './mainBody.js';

const home = () => {
    return (
        <div>
        <Header />
        <MainBody />
        <Footer />
        </div>
    )
}

export default home

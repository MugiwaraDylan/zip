import { Link } from 'react-router-dom';
import Header from './header.js';
import * as ReactBootStrap from 'react-bootstrap';
import Footer from './footer.js';
import LectureCards from './lectureCards.js';
const lectures = () => {
    return (
        <div>
            <Header />
            <LectureCards />
            <Footer />
        </div>
    )
}

export default lectures
